"""Compressed report extraction."""
import zipfile
import gzip


def _zip_archive(self):
    """Perform the unzip action."""
    unzipped_file = zipfile.ZipFile(self.input_filename)
    """Write the file(s) to specified directory"""
    if len(unzipped_file.namelist()) > 1:
        print("Too many reports in this zip.")
        raise
    name = unzipped_file.namelist()[0]
    self.archive_filename = name
    self.archive_content = unzipped_file.read(name)


def _gz_archive(self):
    """Perform the unzip action."""
    unzipped_file = gzip.open(self.input_filename)
    """Write the file(s) to specified directory"""
    self.archive_filename = unzipped_file.name.rstrip('.gz')
    self.archive_content = unzipped_file.read()


class Unzip:
    """Unzip a file to a specified output directory."""

    def __init__(self, input_filename, dir):
        """Attribute needed for file processing.

        Keyword arguments:
        input_filename -- file that needs to be unzipped
        dir -- directory where the file is located
        zipped_files -- array of the unzipped filenames
        """
        self.input_filename = input_filename
        self.dir = dir
        self.archive_filename = ""
        self.archive_content = ""

    def extract(self):
        """Perform the unzip action."""
        if(self.input_filename.find('zip') > 0):
            _zip_archive(self)
            return
        if(self.input_filename.find('gz') > 0):
            _gz_archive(self)
            return
        raise NotImplementedError

    def get_unzipped_filenames(self):
        """Return zipped files."""
        return self.archive_filename

    def get_unzipped_content(self):
        """Return zipped files."""
        return self.archive_content
