"""DMARC XML Parser."""
import xml.etree.ElementTree as ET


class ParseDmarc:
    """Parse a DMARC XML formatted report."""

    def __init__(self, input_filename):
        """Attribute needed for file processing.

        Keyword arguments:
        input_filename -- file that needs to be parsed
        """
        self.input_filename = input_filename

    def parser(self):
        """Open the file to be Parsed and input the XML into the DOM."""
        self.doc = ET.fromstring(self.input_filename)

    def get_report_metadata(self):
        """Extract the DMARC metadata.

        As defined here:
        http://www.dmarc.org/draft-dmarc-base-00-02.txt in Appendix C
        If no data is found, return NA
        """
        date_range_begin = self.doc.findtext("report_metadata/date_range/begin",
                                             default="NA")
        date_range_end = self.doc.findtext("report_metadata/date_range/end",
                                           default="NA")
        return {
            'org_name': self.doc.findtext("report_metadata/org_name",
                                          default="NA"),
            'email': self.doc.findtext("report_metadata/email", default="NA"),
            'extra_contact_info': self.doc.findtext("report_metadata/extra_contact_info",
                                                    default="NA"),
            'report_id': self.doc.findtext("report_metadata/report_id", default="NA"),
            'date_range_begin': int(date_range_begin),
            'date_range_end': int(date_range_end)
        }

    def get_policy_published(self):
        """Extract the DMARC policy published information.

        As defined here:
        http://www.dmarc.org/draft-dmarc-base-00-02.txt in Section 6.2
        If no data is found, return NA
        """
        pct = self.doc.findtext("policy_published/pct", default="NA")

        return {
            'domain': self.doc.findtext("policy_published/domain", default="NA"),
            'adkim': self.doc.findtext("policy_published/adkim", default="NA"),
            'aspf': self.doc.findtext("policy_published/aspf", default="NA"),
            'p': self.doc.findtext("policy_published/p", default="NA"),
            'pct': int(pct)
        }

    def get_records(self):
        """Extract the DMARC records.

        As defined here:
        http://www.dmarc.org/draft-dmarc-base-00-02.txt in Appendix C
        If no data is found, return NA
        """
        container = self.doc.findall("record")
        records = []
        for elem in container:
            count = elem.findtext("row/count", default="NA")
            records.append({
                'source_ip': elem.findtext("row/source_ip", default="NA"),
                'count': int(count),
                'disposition': elem.findtext("row/policy_evaluated/disposition",
                                             default="NA"),
                'dkim': elem.findtext("row/policy_evaluated/dkim", default="NA"),
                'spf': elem.findtext("row/policy_evaluated/spf", default="NA"),
                'type': elem.findtext("row/policy_evaluated/reason/type", default="NA"),
                'comment': elem.findtext("row/policy_evaluated/reason/comment",
                                         default="NA"),
                'header_from': elem.findtext("identifiers/header_from", default="NA"),
                'dkim_domain': elem.findtext("auth_results/dkim/domain", default="NA"),
                'dkim_result': elem.findtext("auth_results/dkim/result", default="NA"),
                'dkim_hresult': elem.findtext("auth_results/dkim/human_result",
                                              default="NA"),
                'spf_domain': elem.findtext("auth_results/spf/domain", default="NA"),
                'spf_result': elem.findtext("auth_results/spf/result", default="NA")
            })

        return records
