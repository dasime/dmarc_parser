"""Formatting reports for the console."""
from datetime import datetime


class Console:
    """Alias human words to console codes."""

    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def _format_boolean(boolean_value):
    """Format a boolean with pretty colour codes.

    Arguments:
    boolean_value -- The string to format. Can be "pass", "fail" or "NA".
    """
    if (boolean_value == "fail"):
        return Console.RED + boolean_value + Console.END
    if (boolean_value == "pass"):
        return Console.GREEN + boolean_value + Console.END
    if (boolean_value == "NA") or (boolean_value == "neutral"):
        return Console.YELLOW + boolean_value + Console.END
    return boolean_value


class Format:
    """Terminal Layouts."""

    def metadata_response(**metadata):
        """Build a metadata response for the console.

        Arguments:
        **metadata -- A dict containing the metadata from the report.
        """
        return f"""
Organisation:     {metadata['org_name']}
Email:            {metadata['email']}
Extra Contact:    {metadata['extra_contact_info']}
Report ID:        {metadata['report_id']}
Date Range Start: {(datetime.utcfromtimestamp(metadata['date_range_begin'])
                            .strftime('%Y-%m-%d %H:%M:%S'))}
Date Range End:   {(datetime.utcfromtimestamp(metadata['date_range_end'])
                            .strftime('%Y-%m-%d %H:%M:%S'))}"""

    def policy_response(**policy):
        """Build a policy response for the console.

        Arguments:
        **policy -- A dict containing the policy data from the report.
        """
        return f"""\
Domain:           {policy['domain']}
ADKIM:            {policy['adkim']}
ASPF:             {policy['aspf']}
P:                {policy['p']}
PCT:              {policy['pct']}"""

    def record_response(**record):
        """Build a record response for the console.

        Arguments:
        **record -- A dict containing a record from the report.
        """
        return f"""
    Source IP:        {record['source_ip']}
    Count:            {record['count']}
    disposition:      {record['disposition']}
    DKIM:             {_format_boolean(record['dkim'])}
    SPF:              {_format_boolean(record['spf'])}
    Type:             {record['type']}
    Comment:          {record['comment']}
    Header From:      {record['header_from']}
    DKIM Domain:      {record['dkim_domain']}
    DKIM Result:      {_format_boolean(record['dkim_result'])}
    DKIM Human:       {_format_boolean(record['dkim_hresult'])}
    SPF Domain:       {record['spf_domain']}
    SPF Result:       {_format_boolean(record['spf_result'])}"""
