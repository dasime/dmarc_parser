# DMARC Viewer

_A Simple DMARC Viewer_

Reads in a compressed DMARC report and "pretty" prints it to the terminal for quick inspection.

Based on the work done by Elmer Thomas in:
<https://github.com/thinkingserious/sendgrid-python-dmarc-parser>.

## Example Usecase

Install the package.

    pip install -e .

Run the script pointing at a DMARC report.

    dmarcviewer $DMARC_REPORT | less

It will print the report to STDOUT. Piping this script to `less` (other pagers are available)
is of course optional - but makes it possible to work with larger reports.

### Is there an easy way to view multiple reports at once?

Doing one thing well is the the unix way. If you want to view multiple reports at once you can use
a command like `find` to run the parser multiple times. For example:

    find . \( -name "*zip" -o -name "*gz" \) -exec dmarcviewer "{}" \; | less

The above command will find any file in the current directory (or below it) that ends
in `zip` or `gz`, execute the script on that file, and pipe the output into `less`
like in the first example.
