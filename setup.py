"""DMARC Viewer setup."""
import io
import os
from setuptools import setup

# Package meta-data.
NAME = 'DMARC Viewer'
DESCRIPTION = 'A simple DMARC viewer.'
URL = 'https://gitlab.com/dasime/dmarcviewer'
EMAIL = 'iam@dasi.me'
AUTHOR = 'David Sime'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = '0.3.1'

PACKAGES = [
    'dmarc_parser',
]

SCRIPTS = [
    'bin/dmarcviewer',
]

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
try:
    with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url=URL,
    author=AUTHOR,
    author_email=EMAIL,
    license='MIT',
    packages=PACKAGES,
    scripts=SCRIPTS,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Communications :: Email',
        'Topic :: Internet :: Log Analysis',
        'Topic :: System :: Networking :: Monitoring',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
    ],
)
