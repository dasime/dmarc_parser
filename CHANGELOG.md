# Changelog

## 0.3.1 - 2019-01-28

### Changed

* Colour the word "neutral" yellow. Observed in Yahoo reports.

## 0.3.0 - 2018-11-26

### Added

* Reintroduced `setup.py` and friends with correct project metadata.

### Updated

* Moved script from `bin/app.py` to `bin/simpledmarc`
* Updated example usecases in README to use the new install entrypoint.

### Changed

* Renamed the package to `dmarcviewer`.

## 0.2.2 - 2018-11-23

### Added

* Adding an example command for using `find` to view multiple reports at once.

### Updated

* No longer write intermediate files between extraction and parsing.

## 0.2.1 - 2018-11-22

### Added

* Added support for `xml.gz` reports.

## 0.2.0 - 2018-11-22

### Added

* New console formatting module.

### Updated

* Added `.editorconfig`.
* PEP and whitespace fixing, general project tidy.
* dmarc_parser now returns objects rather than inserting to MySQL.

### Removed

* Removed project metadata. We'll make some new metadata files when we've finished.
* Removed MySQL and Flask code.

## 0.1.1 - 2014-03-11

### Updated

* Updated setup.py `install_requires` to remove core Python packages. (Glenn Barrett)

## 0.1.0 - 2012-07-20

Initial Commit (Elmer Thomas)
